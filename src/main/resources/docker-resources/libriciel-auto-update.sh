#!/bin/bash

# Libriciel Auto-update
# Copyright (C) 2024 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

VERSION="master"

# Lance une mise a jour de patch si une nouvelle version d'un Libriciel est detectee.

# Nom du script (pour les noms de fichier)
SCRIPT_NAME="libricielAutoUpdate"

OPTION_REGEX1="^(--dry-run|--update|--help|--version)$"
OPTION_REGEX2="^(--now)$"

# testCommands
REQUIRED_COMMANDS=("awk" "sed" "grep" "find" "curl" "tail" "sort" "cat" "rm" "hostname" "jq" "tee")

# poleLibriciel
# testLibricielFolder
LS_LIST=("webactes" "webdelib" "refae" "versae" "asalae" "webgfc" "comelus" "idelibre" "lsmessage" "pastell" "s2low" "iparapheur")

# compareVersionPatch
LS_VERSION_REGEX="^([0-9]|[0-9][0-9]).([0-9]|[0-9][0-9]).([0-9]|[0-9][0-9])$"
LS_SEMVER_REGEX="^[A-Z]+\s(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)$"

# randomExec
DELAY_REGEX="^([0-9]*)$"

# Log
LOG_DATE="$(date '+%Y-%m-%d')"
LOG_FILE="/var/log/${SCRIPT_NAME}_${LOG_DATE}.log"

# libricielUpdate
LS_UPDATE_URL="https://nexus.libriciel.fr/repository/ls-raw/public/libriciel/libriciel-update.sh"

# Get the delay from .env file
# Difference between the release date of the last patch and the delay
# We determine whether we update or not
function randomExec {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    if [[ "${OPTION2}" == "--now" ]]; then
        DELAY=0
	logWarn "Le script sera exécuté sans délai de carence." "${LOG_FILE}"
    else
        # shellcheck source=/opt/pastell/current/.env
        source <(grep 'DELAY=' "${LS_ENV}")
        [[ "${DELAY}" =~ ${DELAY_REGEX} ]] || \
            logErr "La valeur de \"DELAY\" est malformée, vérifier le fichier \"/opt/${LIBRICIEL}/current/.env\". Arrêt du script ⛔️" "${LOG_FILE}"
        logInfo "Délai de carence configuré : ${DELAY} jour(s)." "${LOG_FILE}"
    fi
    RANGE=$((VERSION_AVAILABILTY - DELAY))
    # If the delay is not expired, the script stops
    if [ $RANGE -lt 0 ]; then
        logInfo "Le script ne va pas continuer aujourd'hui ❎" "${LOG_FILE}"
        exit 0
    fi
    RANGE=$((RANGE>6 ? 6 : RANGE))
    RANGE=$((7 - RANGE))
    RANDOM7DAYS=$((RANDOM % RANGE))
    # If the delay is more than 7 days, the script continues
    if [[ "${RANDOM7DAYS}" == 0 ]]; then
        logInfo "Le script va continuer 🚀" "${LOG_FILE}"
    else
        logInfo "Le script ne va pas continuer aujourd'hui ❎" "${LOG_FILE}"
        exit 0
    fi
}

# Outputs the logs
function errNoLog {
    local LOG_MSG="${1}"
    echo -e "\e[31m[ERREUR] $(date '+%Y-%m-%d %H:%M:%S') - [${FUNCNAME[1]}] ${LOG_MSG}\e[0m"
    exit 254
}

function infoNoLog {
    local LOG_MSG="${1}"
    echo -e "\e[34m[INFO] $(date '+%Y-%m-%d %H:%M:%S') - [${FUNCNAME[1]}] ${LOG_MSG}\e[0m"
}

function logErr {
    local LOG_MSG="${1}"
    local LOG_FILE="${2}"
    echo -e "\e[31m[ERREUR] $(date '+%Y-%m-%d %H:%M:%S') - [${FUNCNAME[1]}] ${LOG_MSG}\e[0m" | tee -a "${LOG_FILE}"
    logZip
    exit 255
}

function logInfo {
    local LOG_MSG="${1}"
    local LOG_FILE="${2}"
    echo -e "\e[34m[INFO] $(date '+%Y-%m-%d %H:%M:%S') - [${FUNCNAME[1]}] ${LOG_MSG}\e[0m" | tee -a "${LOG_FILE}"
}

function logWarn {
    local LOG_MSG="${1}"
    local LOG_FILE="${2}"
    echo -e "\e[1;33m[AVERTISSEMENT] $(date '+%Y-%m-%d %H:%M:%S') - [${FUNCNAME[1]}] ${LOG_MSG}\e[0m" | tee -a "${LOG_FILE}"
}

# Zip the log file
function logZip {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    logInfo "Archivage des journaux." "${LOG_FILE}"
    gzip "${LOG_FILE}" --suffix -"$(date '+%H%M%S')".gz
}

# Checks that all commands in a list exist
function testCommands {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    for REQUIRED_COMMAND in "${REQUIRED_COMMANDS[@]}"; do
        command -v "${REQUIRED_COMMAND}" &> /dev/null || logErr "La commande \"${REQUIRED_COMMAND}\" est introuvable. Arrêt du script ⛔️" "${LOG_FILE}"
    done
}

# Check if symbolic link exists
function testDistLink {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local LS_DIST="${1}"
    if [ ! -e "${LS_DIST}" ]; then
        logErr "Le lien symbolique \"${LS_DIST}\" n'existe pas. Arrêt du script ⛔️" "${LOG_FILE}"
    else
        logInfo "Le lien symbolique \"${LS_DIST}\" existe ✅" "${LOG_FILE}"
    fi
}

# Check if file exists
function testEnvFile {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local LS_ENV="${1}"
    if [ ! -e "${LS_ENV}" ]; then
        logErr "Le fichier \"${LS_ENV}\" n'existe pas. Arrêt du script ⛔️" "${LOG_FILE}"
    else
        logInfo "Le fichier \"${LS_ENV}\" existe ✅" "${LOG_FILE}"
    fi
}

# Check that the URL is accessible
function testUrlHttpReturnCode {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local TESTED_URL="${1}"
    if [[ "$(curl --silent --connect-timeout 5 --head "${TESTED_URL}" | grep HTTP | awk '{print $2}')" != "200" ]]; then
	logErr "URL \"${TESTED_URL}\" HS. Arrêt du script ⛔️" "${LOG_FILE}"
    else
        logInfo "URL \"${TESTED_URL}\" OK ✅" "${LOG_FILE}"
    fi
}

# Checks which application is installed on the server
function getApp {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    for LIBRICIEL in "${LS_LIST[@]}"; do
        LIBRICIEL=$(find /opt/ -mindepth 1 -maxdepth 1 -type d | grep -w "${LIBRICIEL}" | sed 's#/opt/##')
        if [ -n "${LIBRICIEL}" ] && [ -d "/opt/${LIBRICIEL}" ]; then
            logInfo "Le libriciel \"${LIBRICIEL}\" est installé et a été détecté automatiquement ✅" "${LOG_FILE}"
            break
        fi
    done
    if [ -z "${LIBRICIEL}" ]; then
        logErr "Aucun libriciel ne semble installé. Vérifiez le répertoire /opt. Arrêt du script ⛔️" "${LOG_FILE}"
    fi
    # testDistLink
    LS_DIST="/opt/${LIBRICIEL}/dist"
    # testEnvFile
    LS_ENV="/opt/${LIBRICIEL}/current/.env"
}

# Check the installed version
function getVersion {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local LIBRICIEL="${1}"
    LS_INSTALLED_VERSION=$(readlink /opt/"${LIBRICIEL}"/dist | sed 's#.*dist-##' | sed 's#/##')
    if [[ "${LS_INSTALLED_VERSION}" =~ ${LS_VERSION_REGEX} ]]; then
        logInfo "Version installée de \"${LIBRICIEL}\" trouvée : ${LS_INSTALLED_VERSION}" "${LOG_FILE}"
    else
        logErr "Ce numéro de version installée n'est pas valide : ${LS_INSTALLED_VERSION}. Arrêt du script ⛔️" "${LOG_FILE}"
    fi
}

# Loop on all poles and determine the application
# Returns the URL of the list of available versions of the application
function getNexusAppUrl {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local LIBRICIEL="${1}"
    case "${LIBRICIEL}" in
        "webactes"|"webdelib")
        LS_POLE="actes"
        logInfo "Le pôle \"${LS_POLE}\" a été trouvé ✅" "${LOG_FILE}"
        ;;
        "refae"|"versae"|"asalae")
        LS_POLE="archivage"
        logInfo "Le pôle \"${LS_POLE}\" a été trouvé ✅" "${LOG_FILE}"
        ;;
        "webgfc"|"comelus"|"idelibre"|"lsmessage")
        LS_POLE="citoyens"
        logInfo "Le pôle \"${LS_POLE}\" a été trouvé ✅" "${LOG_FILE}"
        ;;
        "pastell"|"s2low")
        LS_POLE="plateforme"
        logInfo "Le pôle \"${LS_POLE}\" a été trouvé ✅" "${LOG_FILE}"
        ;;
        "iparapheur")
        LS_POLE="signature"
        logInfo "Le pôle \"${LS_POLE}\" a été trouvé ✅" "${LOG_FILE}"
        ;;
        *)
	logErr "Aucun libriciel ne semble installé. Vérifiez le répertoire /opt. Arrêt du script ⛔️" "${LOG_FILE}"
        ;;
    esac
    LS_URL="https://nexus.libriciel.fr/service/rest/repository/browse/ls-raw/public/${LS_POLE}/${LIBRICIEL}/"
}

# Creates a temporary file with the list of versions by publication date and URL suffix
# Determines the latest latest version and its URL suffix
function getNexusLastLibricielVersionPatch {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    LS_INSTALLED_VERSION_NONPATCH=$(echo "${LS_INSTALLED_VERSION}" | sed --regexp-extended 's/.[0-9]*$//')
    LS_LAST_AVAILABLE_VERSION=$(curl --silent --request 'GET' --header 'accept: application/json' "https://allo.libriciel.fr/api/product/lastCurrentTag/${LIBRICIEL}/${LS_INSTALLED_VERSION_NONPATCH}" | jq --raw-output '.[]')
    [[ "${LS_LAST_AVAILABLE_VERSION}" =~ ${LS_SEMVER_REGEX} ]] && \
       LS_LAST_AVAILABLE_VERSION_PATCH="${BASH_REMATCH[3]}"
    LS_LAST_VERSION=${LS_INSTALLED_VERSION_NONPATCH}.${LS_LAST_AVAILABLE_VERSION_PATCH}
    LS_URL="https://nexus.libriciel.fr/service/rest/v1/search/assets?name=public/${LS_POLE}/${LIBRICIEL}/${LIBRICIEL}-${LS_LAST_VERSION}.tar.gz"
    LS_URL_TGZ_FILE_LAST_MDATE=$(curl --silent --request 'GET' --header 'accept: application/json' "${LS_URL}" | jq --join-output --raw-output ".items[].lastModified")
}

# Check how long ago the latest version was released
# Checks if the delay has been exceeded
function getLastVersionAvailability {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    UNIX_LS_URL_TGZ_FILE_LAST_MDATE=$(date +%s --date "${LS_URL_TGZ_FILE_LAST_MDATE}")
    UNIX_CURRENT_DATE=$(date +%s)
    VERSION_AVAILABILTY=$(((UNIX_CURRENT_DATE-UNIX_LS_URL_TGZ_FILE_LAST_MDATE)/86400))
    logInfo "La version ${LS_LAST_VERSION} du libriciel \"${LIBRICIEL}\" est sortie il y a ${VERSION_AVAILABILTY} jours." "${LOG_FILE}"
}

# Checks the validity of the most recent version number
function testLastVersionRegex {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    local LS_LAST_VERSION=${1}
    [[ "${LS_LAST_VERSION}" =~ ${LS_VERSION_REGEX} ]] || \
        logErr "Ce numéro de version disponible n'est pas valide : ${LS_LAST_VERSION}. Arrêt du script ⛔️" "${LOG_FILE}"
}

# Determines the current version and the most recent version
# Run the update script
function compareVersionAndUpdateIfNeeded {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    [[ "${1}" == "--update" || "${1}" == "--dry-run" ]] || \
        logErr "La fonction doit être appelée soit avec l'option \"--update\" ou soit avec l'option \"--dry-run\", (variable \${OPTION}). Arrêt du script ⛔️" "${LOG_FILE}"
    LS_INSTALLED_VERSION_PATCH=$(echo "${LS_INSTALLED_VERSION}" | sed --regexp-extended 's/([0-9]|[0-9][0-9]).([0-9]|[0-9][0-9]).//')
    LS_LAST_VERSION_PATCH=$(echo "${LS_LAST_VERSION}" | sed --regexp-extended 's/([0-9]|[0-9][0-9]).([0-9]|[0-9][0-9]).//')
    if [[ ${1} == "--update" ]] && [ "${LS_INSTALLED_VERSION_PATCH}" -lt "${LS_LAST_VERSION_PATCH}" ]; then
        logInfo "La version ${LS_LAST_VERSION} du libriciel \"${LIBRICIEL}\" va être installée." "${LOG_FILE}"
        libricielUpdate
    elif [[ ${1} == "--dry-run" ]] && [ "${LS_INSTALLED_VERSION_PATCH}" -lt "${LS_LAST_VERSION_PATCH}" ]; then
        logInfo "Le libriciel \"${LIBRICIEL}\" en version ${LS_INSTALLED_VERSION} peut être mis à jour en version ${LS_LAST_VERSION} ℹ️" "${LOG_FILE}"
    elif [[ "${LS_INSTALLED_VERSION_PATCH}" -eq "${LS_LAST_VERSION_PATCH}" ]]; then
        logInfo "Le libriciel \"${LIBRICIEL}\" est déjà à la version la plus récente disponible. Version ${LS_INSTALLED_VERSION} ✅" "${LOG_FILE}"
    fi
}

# Download the update script
# Launch the update script with the latest version
function libricielUpdate {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    logInfo "Exécution du script libriciel-update.sh 💾" "${LOG_FILE}"
    bash <(curl --connect-timeout 5 -s ${LS_UPDATE_URL}) "${LS_LAST_VERSION}" --force &> /dev/null
    RETURNED_VALUE=$?
    if [ ${RETURNED_VALUE} -ne 0 ]; then
        logErr "Le script libriciel-update.sh a eu une erreur. Arrêt du script ⛔️" "${LOG_FILE}"
    else
        LS_NEW_VERSION=$(readlink /opt/"${LIBRICIEL}"/dist | sed 's#/opt/*.*/dist-##' | sed 's#/##')
        [[ "${LS_NEW_VERSION}" =~ ${LS_VERSION_REGEX} ]] || \
            logErr "Ce numéro de version installée n'est pas valide : ${LS_NEW_VERSION}. Arrêt du script ⛔️" "${LOG_FILE}"
    fi
    if [[ "${LS_NEW_VERSION}" == "${LS_LAST_VERSION}" ]]; then
        logInfo "L'instance est à jour ✅." "${LOG_FILE}"
    fi
}

function help {
    infoNoLog "Début fonction 📖"
echo -e "Script d'installation automatique des patches de Libriciel-SCOP version ${VERSION}.
Options obligatoires : --update
| --update : Compare et lance la mise à jour du patch automatiquement si une version patch supérieure est disponible sur le dépôt.
| --dry-run : Compare sans lancer la mise à jour du patch automatiquement si une version patch supérieure est disponible sur le dépôt. Désactive le délai d'attente aléatoire.
Option facultative pour les tests:
| --now : Exécution sans délai d'attente aléatoire et sans délai de carence.
Option --help : Affiche cette aide.
Option --version : Affiche le numéro de version."
    exit 0
}

# Wait some time before script continues
function waitSomeTime {
    logInfo "Début fonction 🔧" "${LOG_FILE}"
    RANDOM_WAIT=$((RANDOM % 7199))
    if [ "${OPTION}" == "--dry-run" ] || [ "${OPTION2}" == "--now" ]; then
        RANDOM_WAIT=0
	logWarn "Le script sera exécuté sans délai d'attente aléatoire." "${LOG_FILE}"
    else
        logInfo "Le script attend ${RANDOM_WAIT} secondes avant de continuer... ⏳" "${LOG_FILE}"
        sleep "${RANDOM_WAIT}"
        logInfo "Délai d'attente écoulé ⌛️" "${LOG_FILE}"
    fi
}

function main {
    [[ "$(whoami)" == "root" ]] || errNoLog "Le script doit être exécuté en root. Arrêt du script ⛔️"
    [[ -z "${1}" ]] && help
    [[ "${1}" =~ ${OPTION_REGEX1} ]] || errNoLog "Option inconnue : \"${1}\". Voir --help. Arrêt du script ⛔️"
    if [[ -n "${2}" ]]; then
        [[ "${2}" =~ ${OPTION_REGEX2} ]] || errNoLog "Option inconnue : \"${2}\". Voir --help. Arrêt du script ⛔️"
        OPTION2="${2}"
    fi
    case "${1}" in
        "--update")
            OPTION="${1}"
        ;;
        "--dry-run")
            OPTION="${1}"
	    logWarn "Le script sera exécuté sans faire de mise à jour patch effective." "${LOG_FILE}"
        ;;
        "--help")
            help
        ;;
        "--version")
            echo ${VERSION}
            exit 0
        ;;
    esac
    testCommands
    getApp
    testDistLink "${LS_DIST}"
    testEnvFile "${LS_ENV}"
    getVersion "${LIBRICIEL}"
    getNexusAppUrl "${LIBRICIEL}"
    testUrlHttpReturnCode "${LS_UPDATE_URL}"
    waitSomeTime "${OPTION2}"
    getNexusLastLibricielVersionPatch
    getLastVersionAvailability
    randomExec
    testLastVersionRegex "${LS_LAST_VERSION}"
    compareVersionAndUpdateIfNeeded "${OPTION}"
    logZip
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  main "$@"
fi
